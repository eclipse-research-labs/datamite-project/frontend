<!--
  ~ Copyright (c) 2024 LOBA
  ~ 
  ~ Permission is hereby granted, free of charge, to any person obtaining a copy of
  ~ this software and associated documentation files (the "Software"), to deal in
  ~ the Software without restriction, including without limitation the rights to
  ~ use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  ~ the Software, and to permit persons to whom the Software is furnished to do so,
  ~ subject to the following conditions:
  ~ 
  ~ The above copyright notice and this permission notice shall be included in all
  ~ copies or substantial portions of the Software.
  ~ 
  ~ THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  ~ IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  ~ FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  ~ COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  ~ IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  ~ CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  ~ 
  ~ SPDX-License-Identifier: MIT
  ~ 
  ~ Contributors:
  ~     [name] - [contribution]
-->

# DESCRIPTION

This project contains the development for the front-end of DATAMITE.

![architecture_DGB](docs/wireframes.jpg)


## SYSTEM REQUIREMENTS
- Node version TBD
- Web server


## PROJECT DEPENDENCIES
- Keycloak
- CMS
- API Data Catalogue
- API Data Glossary
- API Data Dictionary
- API other (support tools)


## DEPLOY
Available soon


## ROADMAP

- [x] Frontend Wireframes
- [x] UX for managing Data Catalogue + Data Glossary + Data Dictionary
- [x] Front-end modular arquitecture

- [ ] UI Design for managing Data Catalogue + Data Glossary + Data Dictionary

- [ ] Data Catalogue UI development
- [ ] Data Glossary UI development
- [ ] Data Dictionary UI development

- [ ] Integration of APIs on front-end
- [ ] CMS Integration
- [ ] Data Catalogue API Integration
- [ ] Data Glossary API Integration
- [ ] Data Dictionary API Integration

- [ ] Modular deployment

## LICENSE
In principle this component is distributed under MIT license.


## SUPPORT
- hugopeixoto@loba.com
- edgar@loba.com
