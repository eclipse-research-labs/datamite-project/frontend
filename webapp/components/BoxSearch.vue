<!--
  ~ Copyright (c) 2024 LOBA
  ~ 
  ~ Permission is hereby granted, free of charge, to any person obtaining a copy of
  ~ this software and associated documentation files (the "Software"), to deal in
  ~ the Software without restriction, including without limitation the rights to
  ~ use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  ~ the Software, and to permit persons to whom the Software is furnished to do so,
  ~ subject to the following conditions:
  ~ 
  ~ The above copyright notice and this permission notice shall be included in all
  ~ copies or substantial portions of the Software.
  ~ 
  ~ THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  ~ IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  ~ FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  ~ COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  ~ IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  ~ CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  ~ 
  ~ SPDX-License-Identifier: MIT
  ~ 
  ~ Contributors:
  ~     [name] - [contribution]
-->

<template>
    <div class="modal-box-search">
        <h3>{{ searchTile }}</h3>
        <div class="search-wrapper">
            <input type="text" v-model="searchTerm" class="input-search" @input="onSearch">
        </div>
    </div>
    <ul class="modal-box-checkbox-list modal-box-checkbox-list">
        <li v-for="domain in visibleDomains" :key="domain?.name">
            <input type="checkbox" :id="`${domain?.name}`" :checked="selectedDomains[domain?.name]" @change="toggleDomainSelection(domain)"/>
            <label :for="`${domain?.name}`">{{ domain?.name }}</label>
        </li>
        <li v-if="!showAllResults && searchTerm === '' && filteredResults.length > 5">
            <span class="see-more-domains" @click="showAllResults = true">
            <i class="icon-add add"></i>
            {{ filteredResults.length - 5 }}
            <i class="icon-arrow-down arrow"></i>
            </span>
        </li>
    </ul>
</template>

<script setup lang="ts">
import type { platformArray, Domain } from '~/interfaces/types';

// - domains: Array that contains information about different domains (platforms, data types, etc.)
const props = defineProps({
    searchTile: String,
    domains: {
        type: Array as PropType<Domain[]>,
        required: true,
    },
    selectedDomains: {
      type: Array as PropType<Domain[]>,
      default: () => [],
    }
});

const searchTerm = ref(''); // Stores the search input
const showAllResults = ref(false); // Tracks if the "See More" has been clicked
const selectedDomains = ref<{ [key: string]: boolean }>({}); 

const emit = defineEmits(['updateSelectedDomains']);

// Set initial selected domains based on the prop
onMounted(() => {
    setInitialSelectedDomains();
});

// Watch for changes in `selectedDomains` prop and update reactive state
watch(
    () => props.selectedDomains,
    () => {
        setInitialSelectedDomains();
    },
    { immediate: true }
);

function setInitialSelectedDomains() {
  selectedDomains.value = {};

  // Initialize selected domains as a dictionary with domain names as keys
  props.domains.forEach(domain => {
      const isSelected = props.selectedDomains.some(selectedDomain => selectedDomain.name === domain.name);
      selectedDomains.value[domain.name] = isSelected;
  });
}

// Computed property to filter domains based on the searchTerm
const filteredResults = computed(() => {
    if (!searchTerm.value) return props.domains; // If there's no search input, return all domains
    return props.domains.filter(domain =>
        domain.name.toLowerCase().includes(searchTerm.value.toLowerCase())
    );
});

// Computed property to show only the first 5 domains if no searchTerm is present and showAllResults is false
const visibleDomains = computed(() => {
  if (searchTerm.value !== '') {
    // Show all filtered domains if a search is happening
    return filteredResults.value;
  } else if (showAllResults.value || filteredResults.value.length <= 5) {
    // If "See More" has been clicked or fewer than 5 domains exist, show all
    return filteredResults.value;
  }
  // Otherwise, show only the first 5
  return filteredResults.value.slice(0, 5);
});

function toggleDomainSelection(domain: Domain) {
    // Toggle the value in the selectedDomains object
    selectedDomains.value[domain.name] = !selectedDomains.value[domain.name];

    // Create an array of selected domain objects based on selectedDomains state
    const selectedDomainObjects = props.domains.filter(d => selectedDomains.value[d.name]);
    
    // Emit the selected domains back to the parent component
    emit('updateSelectedDomains', selectedDomainObjects);
}

// Method to clear selected checkboxes
function clearSelection() {
    selectedDomains.value = {}; // Reset all selected domains
    searchTerm.value = '';       // Reset the search term
}

// Expose the clearSelection function to be accessible from the parent component
defineExpose({ clearSelection });

function onSearch() {
  // If the search term is cleared, reset the "show more" state for all vocabularies
  if (!searchTerm.value) {
    showAllResults.value = false; // Reset to initial state (not showing all terms)
  }
}

</script>