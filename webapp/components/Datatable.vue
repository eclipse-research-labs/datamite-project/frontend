<!--
  ~ Copyright (c) 2024 LOBA
  ~ 
  ~ Permission is hereby granted, free of charge, to any person obtaining a copy of
  ~ this software and associated documentation files (the "Software"), to deal in
  ~ the Software without restriction, including without limitation the rights to
  ~ use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  ~ the Software, and to permit persons to whom the Software is furnished to do so,
  ~ subject to the following conditions:
  ~ 
  ~ The above copyright notice and this permission notice shall be included in all
  ~ copies or substantial portions of the Software.
  ~ 
  ~ THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  ~ IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  ~ FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  ~ COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  ~ IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  ~ CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  ~ 
  ~ SPDX-License-Identifier: MIT
  ~ 
  ~ Contributors:
  ~     [name] - [contribution]
-->

<template>

    <FiltersTop :data="data">
        <slot name="default"></slot>
    </FiltersTop>

    <slot name="quality"></slot>
    <!-- Show this message when there are no filteredRows -->
    <div v-if="filteredRows.length === 0" class="no-results-message">
        No results found.
    </div>

    <div class="datatable" :class="data.options.tableName" v-if="filteredRows && filteredRows.length > 0" @refreshData="handleRefreshData">
        <div class="datatable-head">
            <div v-for="(column, index) in data.columns" key="column.name" :style="`width: ${column.width}`"
                class="datatable-head-column" :class="`datatable-head-column-${column.name}`">
                <label v-if="index === 0" class="form-group-checkbox">
                    <input type="checkbox" />
                    <div class="checkbox__checkmark"></div>
                </label>
                <span v-if="!column.hideLabel">{{ column.label }}</span>
            </div>
        </div>
        <div class="datatable-body">
            <div v-for="row in filteredRows" key="row.id" class="datatable-body-row" :class="rowClass(row)">
                <div v-for="(column, index) in data.columns" key="column.name" :style="`width: ${column.width}`"
                    :class="`datatable-body-row-column-${column.name}`" class="datatable-body-row-column" @click="goDetails($event, row.id)">
                    <Fragment v-if="index === 0 && data.options.allowCheckRow">
                        <label class="form-group-checkbox">
                            <input type="checkbox" v-model="row.is_selected"
                                :checked="row.is_selected ? row.is_selected : false" />
                            <div class="checkbox__checkmark"></div>
                        </label>
                    </Fragment>
                    <span v-if="!column.renderComponent">{{ row[column.name] }}</span>

                    <!-- combine column -->
                    <Fragment v-if="column.combineColumn">
                        <component v-if="column.renderComponent" :is="components[column.renderComponent]"
                            v-bind="{ value: row[column.name], ...componentProps[column.renderComponent], combinedValue: row[column.combineColumn] }">
                        </component>
                    </Fragment>

                    <Fragment v-if="!column.combineColumn">
                        <component v-if="column.renderComponent" :is="components[column.renderComponent]"
                            v-bind="{ value: row[column.name], ...componentProps[column.renderComponent] }"></component>
                    </Fragment>

                    <div v-if="column.name === 'actions'" class="datatable-actions">
                        <button v-if="data.options.favourites" @click="toggleFavorite(row.id)"
                            class="button-icon small-btn favourite" :class="row.is_favourite ? 'active' : ''"><i
                                class="icon-favourites-line"></i></button>
                        <button v-if="data.options.previewComponent" class="preview"
                            :class="row.is_preview ? 'active' : ''" @click="row.is_preview = !row.is_preview"><i
                                class="icon-arrow-down"></i></button>
                    </div>
                </div>
                <div v-if="row.is_preview && data.options.previewComponent" class="datatable-body-row-preview">
                    <component :is="components[data.options.previewComponent]" v-bind="{ row: row }" @open-edit-modal="handleOpenEditModal"></component>
                </div>
            </div>
        </div>
    </div>
</template>
<script setup lang="ts">
import type DataTableI from '~/interfaces/datatable';
import FiltersTop from './FiltersTop.vue';
import { ref } from 'vue';

const props = defineProps<{ data: DataTableI }>()
const emit = defineEmits(['open-edit-modal', 'refreshData']);

const searchQuery = ref<string>('');

const filters = ref({
  name: '',
  description: '',
  shortDescription: ''
});

const filterOpen = ref(false);

const handleOpenEditModal = (row: any) => {
  emit('open-edit-modal', row);
};

// Reactive property to store the selected sort configuration (column and order)
const sortConfig = ref<{ column: string, order: 'asc' | 'desc' }>({
    column: 'updated_at',
    order: 'desc'
});

const sortOptions = ref([
    {
        name: 'Last updated',
        column: 'updated_at',
        order: 'desc'
    },
    {
        name: 'Recent first',
        column: 'created_at',
        order: 'asc'
    },
    {
        name: 'Older first',
        column: 'created_at',
        order: 'desc'
    },
]);

const sortOptionSelected = ref(null);

// Handle sort change event
const handleSort = (selectedOption: any) => {
    if (selectedOption && selectedOption.column && selectedOption.order) {
        sortConfig.value.column = selectedOption.column;
        sortConfig.value.order = selectedOption.order;
    }
};

// Handle filter application// Function to handle the event when filters are applied
const handleApplyFilters = (newFilters: { name: string, description: string, shortDescription: string }) => {
  // Update filters
  filters.value = newFilters;
};

// Computed property to filter and sort rows
const filteredRows = computed(() => {
  let rows = props.data.rows ?? [];

  // Filter rows based on search query and filters
  if (searchQuery.value) {
    const query = searchQuery.value.toLowerCase();
    rows = rows.filter(row => {
      return props.data.columns.some(column => {
        const cellValue = row[column.name];
        return cellValue && cellValue.toString().toLowerCase().includes(query);
      });
    });
  }

  // Filter by filters from FiltersArea
  if (filters.value.name) {
    rows = rows.filter(row => row.name.toLowerCase().includes(filters.value.name.toLowerCase()));
    }
    if (filters.value.description) {
    rows = rows.filter(row => row.description.toLowerCase().includes(filters.value.description.toLowerCase()));
    }
    if (filters.value.shortDescription) {
    rows = rows.filter(row => row.shortDescription.toLowerCase().includes(filters.value.shortDescription.toLowerCase()));
    }

  // Sort rows based on sortConfig
  rows = [...rows].sort((a, b) => {
    const column = sortConfig.value.column;
    const order = sortConfig.value.order;

    const valueA = a[column];
    const valueB = b[column];

    if (valueA === valueB) return 0;
    return order === 'asc' ? (valueA > valueB ? 1 : -1) : (valueA < valueB ? 1 : -1);
  });

  return rows;
});

// Function to toggle the filterOpen state
const toggleFilterOpen = () => {
    filterOpen.value = !filterOpen.value;
};

// include here all components that are used in the table
const components: any = {
    'UserProfile': resolveComponent('UserProfile'),
    'TableColumnsQuality': resolveComponent('TableColumnsQuality'),
    'CatalogDetails': resolveComponent('CatalogDetails'),
    'GlossaryDetails': resolveComponent('GlossaryDetails'),
    'ArtifactDetails': resolveComponent('ArtifactDetails'),
    'TableColumnsName': resolveComponent('TableColumnsName'),
    'TermsDetails': resolveComponent('TermsDetails')
}

const handleRefreshData = () => {
    console.log('datatable refreshData');
  emit('refreshData'); // Bubble up the event to the parent
};

// for components used in the table, add here the props
const componentProps: any = {
    'UserProfile': {
        size: 'xs'
    }
}

// create a function to toggle a favorite for a datatable row item, use the id to store the favorite
const toggleFavorite = (id: Number) => {

    props.data.rows?.forEach((row: any) => {
        if (row.id === id) {
            row.is_favourite = !row.is_favourite;
            // save where ?
        }
    });

}

const goDetails = (event: any, id: Number) => {

    if( ! props.data.options.detailsPath ) return;

    if (!event.target || !event.target.tagName.toLowerCase().includes('button')
        && !event.target.tagName.toLowerCase().includes('input')
        && !event.target.classList.contains('icon-arrow-down')
        && !event.target.classList.contains('checkbox__checkmark')) {
        navigateTo(props.data.options.detailsPath + id);
    }

}

// create a computed property to host the class for a row
const rowClass = computed(() => {
    return (row: any) => {
        return {
            'row--selected': row.is_selected,
            'row--preview': row.is_preview
        }
    }
});


</script>