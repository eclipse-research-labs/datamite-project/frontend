<!--
  ~ Copyright (c) 2024 LOBA
  ~ 
  ~ Permission is hereby granted, free of charge, to any person obtaining a copy of
  ~ this software and associated documentation files (the "Software"), to deal in
  ~ the Software without restriction, including without limitation the rights to
  ~ use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  ~ the Software, and to permit persons to whom the Software is furnished to do so,
  ~ subject to the following conditions:
  ~ 
  ~ The above copyright notice and this permission notice shall be included in all
  ~ copies or substantial portions of the Software.
  ~ 
  ~ THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  ~ IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  ~ FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  ~ COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  ~ IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  ~ CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  ~ 
  ~ SPDX-License-Identifier: MIT
  ~ 
  ~ Contributors:
  ~     [name] - [contribution]
-->

<template>
    <form id="dataset-step-1" @submit.prevent="handleSubmit">
        <div class="title">
            <span class="step-number">1</span>
            <h2>Dataset info</h2>
            <p>You can create new datasets using either the Bulk or Streaming method.</p>
            <p>Choose the option that best suits your needs and get started now!</p>
        </div>
        <div class="step-border">
            <div class="field field-modal-big-label">
                <label for="" class="label"><span>1.</span>Dataset name</label>
                <input type="text" name="name" required v-model="datasetData.name" data-error-message="Fill in this field."/>
                <span class="max-characters">
                    <span class="text">Max. 30 characters</span><span class="separator"></span> <button>
                        <i class="icon-delete"></i></button>
                </span>
            </div>
        </div>
        <div class="step-border">
            <div class="field field-modal-big-label">
                <label for="" class="label"><span>2.</span>Fill in with the Basic Info</label>
            </div>
            <div class="modal-boxs-wrapper">
                <div class="modal-input col-100">
                    <label for="full-description">Full description</label>
                    <input type="text" name="full-description" id="full-description" required v-model="datasetData.description" data-error-message="Fill in this field."/>
                </div>
                <div class="modal-input col-100">
                    <label for="short-description">Short description</label>
                    <input type="text" name="short-description" id="short-description" required v-model="datasetData.summary" data-error-message="Fill in this field."/>
                </div>
            </div>
            <div class="modal-boxs-wrapper">
                <div class="modal-box col-100">
                    <BoxSearch searchTile="Domains" :domains="domains" :selectedDomains="datasetData?.domains" @updateSelectedDomains="updateSelectedDomains" />
                </div>
            </div>
            <div class="field">
                <div class="modal-box">
                    <div class="modal-box-search">
                        <h3>Glossary/Terms</h3>
                        <div class="search-wrapper">
                            <input type="text" v-model="searchTermVocabularies" name="platform-search" class="input-search" @input="onSearch">
                        </div>
                    </div>
                    <div class="modal-list">
                        <div class="modal-list-item" v-for="vocabulary in vocabularies"
                            :key="vocabulary?.id" @click="toggleClass(vocabulary.id)"
                            :class="{ active: isActive(vocabulary?.id) || hasMatchingTerm(vocabulary) }">
                            <div class="item-title">
                                <label class="form-group-checkbox">
                                    <input type="checkbox" @click.stop />
                                    <div class="checkbox__checkmark"></div>
                                </label>
                                <h3>{{ vocabulary?.name }} <sup v-if="vocabulary?.total > 0">{{
                                    vocabulary?.total }}</sup></h3>
                                <i class="icon-arrow-down"></i>
                            </div>
                            <div class="list" @click.stop>
                                <ul class="modal-box-checkbox-list modal-box-checkbox-list">
                                    <li v-for="term in visibleTerms(vocabulary)" :key="term?.id">
                                        <input type="checkbox" :id="`term-${vocabulary?.id}-${term?.id}`" 
                                            @click.stop="toggleTermSelection(vocabulary?.id, term?.id)"
                                            :checked="isTermChecked(vocabulary?.id, term?.id)" />
                                        <label :for="`term-${vocabulary?.id}-${term?.id}`">{{ term?.name }}</label>
                                    </li>

                                    <!-- See More button to display the rest of the terms -->
                                    <li v-if="!isShowingAll[vocabulary.id] && filteredTerms(vocabulary).length > 5">
                                        <span class="see-more-terms" @click="showMoreTerms(vocabulary.id)">
                                            <i class="icon-add add"></i>
                                            {{ filteredTerms(vocabulary).length - 5 }}
                                            <i class="icon-arrow-down arrow"></i>
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <ClientOnly>
        <Teleport to="#modals-container">
            <ModalsModalFeedback v-if="showFeedback" :feedback="feedbackMessage" @toggle="close"/>
        </Teleport>
    </ClientOnly>
</template>

<script setup lang="ts">

import { ref, defineEmits, defineExpose } from 'vue';
import type { PropType } from 'vue';
import type { vocabulariesArray, Domain } from '~/interfaces/types';
import { generateUUID, highlightInvalidFields, updateLoadingUI, clearAllValidationErrors } from '~/utils/utils';
import BoxSearch from '../BoxSearch.vue';

// Define component props with validation and type checks
const props = defineProps({
    currentStep: Number, // Current step in the form wizard
    id: {
      type: String,
      required: false,
    },
    isEditMode: {
      type: Boolean,
      required: false,
    },
    nextStep: {
      type: Function,    // Function to navigate to the next step
      required: true,    // Ensure the nextStep function is passed
    },
    domains: {
        type: Array as PropType<Domain[]>, // List of domains
        required: true,   // Ensure domains array is provided
    },
    vocabularies: {
        type: Array as PropType<vocabulariesArray[]>, // List of vocabularies and terms
        required: true,   // Ensure vocabularies array is provided
    },
     // Flag to indicate if the component is in edit mode
});

// Define an event emitter for the 'datasetCreated' event to notify the parent component
const emit = defineEmits(['datasetCreated', 'updateData']);

// Reactive variables to track catalog ID, created dataset ID, and dataset name
const catalog_id = ref('');
const createdDatasetId = ref('');
const datasetName = ref('');
const checkedDomains = ref<{ name: string }[]>([]);
const checkedTerms = ref<{ [vocabularyId: string]: Set<number> }>({});
const datasetData = ref({
    name: '',
    description: '',
    summary: '',
    domains: [],
    terms: []

});
const feedbackMessage = ref({ type: '', message: '' });
const showFeedback = ref(false);
const checkedDomainsTotal = ref([]);
const selectedTermsTotal = ref([]);

// Watch for changes and emit the lengths
watch([checkedDomainsTotal, selectedTermsTotal], () => {
  emit('updateData', { checkedDomainsLength: checkedDomainsTotal.value.length, selectedTermsLength: selectedTermsTotal.value.length });
});

const close = () => {
    showFeedback.value = false;
};

const fetchDataset = async () => {
    if (props.isEditMode && props.id) {
        try {
            const response = await useFetchAuth(`/catalogs/${props.id}`, { method: 'GET' });
            const data = Array.isArray(response) ? response[0] : response;

            datasetData.value = {
                name: data.name || '',
                description: data.description || '',
                summary: data.summary || '',
                domains: data.domains || [],
                terms: data.terms || [],
            }

            // Update checkedTerms based on the fetched term IDs
            checkedTerms.value = {};

            // Update checkedTerms based on datasetData.terms
            // This ensures that terms in datasetData.terms will be marked as selected
            datasetData.value.terms.forEach((datasetTerm: { id: number }) => {
                // Loop through vocabularies and their terms
                props.vocabularies.forEach((vocabulary: any) => {
                    vocabulary.terms.forEach((vocabularyTerm: { id: number }) => {
                        // If the term.id matches, add it to checkedTerms
                        if (datasetTerm.id === vocabularyTerm.id) {
                            if (!checkedTerms.value[vocabulary.id]) {
                                checkedTerms.value[vocabulary.id] = new Set<number>();
                            }
                            checkedTerms.value[vocabulary.id].add(vocabularyTerm.id);
                        }
                    });
                });
            });
        } catch (error) {
            console.error('Error fetching dataset data:', error);
        }
    }else{
        datasetData.value = {
            name: '',
            description: '',
            summary: '',
            domains: [],
            terms: []
        };

        checkedTerms.value = {}; // Reset checkedTerms when not in edit mode
    }
};

watch(
    () => props.vocabularies,
    (newVocabularies, oldVocabularies) => {
        if (newVocabularies !== oldVocabularies) {
            console.log('Vocabularies updated, re-fetching dataset data.');
            fetchDataset();
        }
    },
    { immediate: true } // Trigger on initial mount
);

onMounted(() => {
    fetchDataset();
});
const updateSelectedDomains = (domains:  { name: string }[]) => {
    checkedDomains.value = domains; // Update checkedDomains with the selected domains
};

// Manage active vocabularies (selected vocabularies) using a Set
const activeVocabularies = ref(new Set<number>());

/**
 * Toggles the given vocabulary ID in the activeVocabularies set.
 * If the vocabulary ID is already in the set, it is removed.
 * If the vocabulary ID is not in the set, it is added.
 * @param {number} id - the vocabulary ID
 */
const toggleClass = (id: number) => {
    if (activeVocabularies.value.has(id)) {
        activeVocabularies.value.delete(id);
    } else {
        activeVocabularies.value.add(id);
    }
};

/**
 * Returns true if the given vocabulary ID is in the activeVocabularies set and
 * false otherwise.
 * @param {number} id - the vocabulary ID to check
 * @return {boolean} whether the vocabulary ID is in the activeVocabularies set
 */
const isActive = (id: number) => {
    return activeVocabularies.value.has(id);
};

/**
 * Toggle the selection of a term by its vocabulary ID and term ID.
 * @param {string | number} vocabularyId - The ID of the vocabulary.
 * @param {number} termId - The ID of the term.
 */
 function toggleTermSelection(vocabularyId: string | number, termId: number) {
  if (!checkedTerms.value[vocabularyId]) {
    checkedTerms.value[vocabularyId] = new Set<number>();
  }

  const termsSet = checkedTerms.value[vocabularyId];

  if (termsSet.has(termId)) {
    termsSet.delete(termId);
  } else {
    termsSet.add(termId);
  }
}

/**
 * Check if a term is selected based on its vocabulary ID and term ID.
 * @param {string | number} vocabularyId - The ID of the vocabulary.
 * @param {number} termId - The ID of the term.
 * @returns {boolean} - True if the term is checked, false otherwise.
 */
 function isTermChecked(vocabularyId: string | number, termId: number) {
  return checkedTerms.value[vocabularyId]?.has(termId) ?? false;
}


/**
 * Handles form submission for Step 1 of the dataset creation process.
 *
 * Validates the form first, and if invalid, highlights the invalid fields.
 * If the form is valid, it proceeds with submitting the form data to the
 * server. If the submission is successful, it clears the form, resets
 * validation errors, and moves to the next step.
 *
 * @return {Promise<void>} A promise that resolves when the submission is
 *     complete, or rejects if there is an error.
 */
const handleSubmit = async () => {
    const form = document.getElementById('dataset-step-1') as HTMLFormElement;

    if (props.currentStep === 1 && form) {
        // First, validate the form and stop further execution if invalid
        if (!form.checkValidity()) {
            // Report validity and highlight invalid fields
            form.reportValidity();
            highlightInvalidFields(form);
            console.log('Form is invalid, showing error messages or highlighting fields.');
            return; // Prevent further execution if the form is invalid
        }

        // If form is valid, proceed with data submission
        const formData = new FormData(form);
        const uuid = generateUUID();
        catalog_id.value = uuid;

        datasetName.value = formData.get('name') as string;

        const data = {
            name: formData.get('name') as string,
            description: formData.get('full-description') as string,
            summary: formData.get('short-description') as string,
            metadata: {
                identifier: '',
                title: formData.get('name') as string,
                description: '',
                creator: 'default creator',
                created: new Date().toISOString(),
            },
            catalogId: props.isEditMode ? props.id : catalog_id.value,
        };

        // Show loading state
        updateLoadingUI(true);

        try {

            if (props.isEditMode && props.id) {
                // Update the dataset
                await submitDataUpdate(data, props.id);
                console.log('Data updated successfully: (request)', data);
            }else{
                // Submit the data
                await submitData(data);
                console.log('Data submitted successfully: (request)', data);
                // Clear the form and validation errors after successful submission
                form.reset();
            }
            
            clearAllValidationErrors(form);

            // Move to the next step
            //props.nextStep();
        } catch (error) {
            console.error('Error during data submission:', error);
        } finally {
            // Always stop the loading indicator
            updateLoadingUI(false);
        }
    } else {
        props.nextStep(); // If not step 1, simply go to the next step
    }
};

// Expose handleSubmit method to the parent
defineExpose({
    handleSubmit
});

// submit form data to API
const submitData = async (data: any) => {
    try {
        useFetchAuth('/catalogs', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then((response: any) => {
            if(response.id){
                // store dataset id
                createdDatasetId.value = response.id;

                for (let i = 0; i < checkedDomains.value.length; i++) {
                    associateDomains(checkedDomains.value[i]?.name, response.id);
                }

                // Prepare the list of selected term IDs only
                const selectedTerms: number[] = [];
                for (const termsSet of Object.values(checkedTerms.value)) {
                    for (const termId of termsSet) {
                        selectedTerms.push(termId);
                    }
                }

                for (let i = 0; i < selectedTerms.length; i++) {
                    associateTerms(selectedTerms[i].toString(), response.id);
                }

                emit('datasetCreated', { createdDatasetId: createdDatasetId.value, datasetName: datasetName.value });
                emit('updateData', { checkedDomainsLength: checkedDomains.value.length, selectedTermsLength: selectedTerms.length });

                showFeedback.value = true;
                feedbackMessage.value = {
                    type: 'success',
                    message: 'Step 1 data added successfully!',
                };
                setTimeout(() => {
                    feedbackMessage.value = { type: '', message: '' };
                    showFeedback.value = false;
                    props.nextStep();
                }, 3000);
            }
        }).catch((error) => {
            const errorData = error.response._data.message || error;
            showFeedback.value = true;
            feedbackMessage.value = {
                type: 'error',
                message: 'Error adding data on step 1:' + errorData,
            };
        });
        
    } catch (error: any) {

        showFeedback.value = true;
        feedbackMessage.value = {
            type: 'error',
            message: 'Error adding data on step 1:' + error,
        };
    }
};

const submitDataUpdate = async (data: any, id: string) => {
    try {
        const responseOld = await useFetchAuth(`/catalogs/${id}`) as { domains: any, terms: any };
        
        if (responseOld?.domains?.length) {
            await Promise.all(
                responseOld.domains.map((domain: any) => removeAssociatedDomains(domain.name, id))
            );
        }

        if (responseOld?.terms?.length) {
            await Promise.all(
                responseOld.terms.map((term: any) => removeAssociatedTermsEntities(term.id, term.relationId))
            );
        }

        useFetchAuth('/catalogs/' + id, { 
            method: 'PUT', 
            headers: {
            'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then((response: any) => {
            for (let i = 0; i < checkedDomains.value.length; i++) {
                associateDomains(checkedDomains.value[i]?.name, id);
            }

            const selectedTerms: number[] = [];
            for (const termsSet of Object.values(checkedTerms.value)) {
                for (const termId of termsSet) {
                    selectedTerms.push(termId);
                }
            }

            for (let i = 0; i < selectedTerms.length; i++) {
                associateTerms(selectedTerms[i].toString(), id);
            }

            emit('datasetCreated', { createdDatasetId: id, datasetName: datasetName.value });
            emit('updateData', { checkedDomainsLength: checkedDomains.value.length, selectedTermsLength: selectedTerms.length });

            showFeedback.value = true;
            feedbackMessage.value = {
                type: 'success',
                message: 'Step 1 data updated successfully!',
            };
            setTimeout(() => {
                feedbackMessage.value = { type: '', message: '' };
                showFeedback.value = false;
                props.nextStep();
            }, 3000);
        }).catch((error) => {
            const errorData = error.response._data.message || error;
            showFeedback.value = true;
            feedbackMessage.value = {
                type: 'error',
                message: 'Error updating data on step 1:' + errorData,
            };
        });

    } catch (error) {
        showFeedback.value = true;
        feedbackMessage.value = {
            type: 'error',
            message: 'Error updating data:' + error,
        };
        console.error('Error during update:', error);
    }
}

const searchTermVocabularies = ref('');
// Track which vocabularies have their terms fully expanded
const isShowingAll = ref<{ [key: string | number]: boolean }>({});



// Function to toggle showing all terms for a specific vocabulary
function showMoreTerms(vocabularyId: string | number) {
  isShowingAll.value[vocabularyId] = true; // Set the vocabulary's visibility to true (show all)
}

// Function to filter terms within a vocabulary based on the global search term
const filteredTerms = (vocabulary: any) => {
  if (!searchTermVocabularies.value) return vocabulary.terms; // If no search, return all terms for the vocabulary
  return vocabulary.terms.filter((term: { name: string }) =>
    term.name.toLowerCase().includes(searchTermVocabularies.value.toLowerCase())
  );
};

function onSearch() {
  // If the search term is cleared, reset the "show more" state for all vocabularies
  if (!searchTermVocabularies.value) {
    isShowingAll.value = {}; // Reset to initial state (not showing all terms)
  }
}

// Function to return only the visible terms (first 5 or all if "See More" is clicked)
const visibleTerms = (vocabulary: any) => {
  const terms = filteredTerms(vocabulary);
  if (isShowingAll.value[vocabulary.id] || terms.length <= 5) {
    return terms; // Show all terms if "See More" was clicked or if there are 5 or fewer terms
  }
  return terms.slice(0, 5); // Otherwise, show only the first 5
};

// Function to check if any term in a vocabulary matches the search term
const hasMatchingTerm = (vocabulary: any) => {
  return filteredTerms(vocabulary).length > 0; // If there are filtered terms, mark as active
};

</script>