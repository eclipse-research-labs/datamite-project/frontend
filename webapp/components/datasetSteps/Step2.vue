<!--
  ~ Copyright (c) 2024 LOBA
  ~ 
  ~ Permission is hereby granted, free of charge, to any person obtaining a copy of
  ~ this software and associated documentation files (the "Software"), to deal in
  ~ the Software without restriction, including without limitation the rights to
  ~ use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  ~ the Software, and to permit persons to whom the Software is furnished to do so,
  ~ subject to the following conditions:
  ~ 
  ~ The above copyright notice and this permission notice shall be included in all
  ~ copies or substantial portions of the Software.
  ~ 
  ~ THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  ~ IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  ~ FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  ~ COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  ~ IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  ~ CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  ~ 
  ~ SPDX-License-Identifier: MIT
  ~ 
  ~ Contributors:
  ~     [name] - [contribution]
-->

<template>
    <form>
        <div class="title">
            <span class="step-number" v-if="!datasetId">2</span>
            <span class="step-number" v-if="datasetId">1</span>
            <h2>Method</h2>
            <p>You can create new datasets using either the Bulk or Streaming method.</p>
            <p>Choose the option that best suits your needs and get started now!</p>
        </div>
        <div class="field field-modal-big-label">
            <label for="" class="label"><span>1.</span>Method</label>
            <ul class="modal-box-checkbox-list modal-box-checkbox-list-ball modal-box-checkbox-list-ball-big">
                <li>
                    <input type="radio" id="bulk" value="bulk" :checked="selectedMethod === 'bulk'"
                        @change="emitMethodChange('bulk')" name="bulk" />
                    <label for="bulk" tabindex="bulk">Bulk</label>
                </li>
                <li>
                    <input type="radio" id="streaming" value="streaming" :checked="selectedMethod === 'streaming'"
                        @change="emitMethodChange('streaming')" name="streaming" />
                    <label for="streaming" tabindex="streaming">Streaming</label>
                </li>
                <li>
                    <input type="radio" id="database-connection" value="database"
                        :checked="selectedMethod === 'database'" @change="emitMethodChange('database')"
                        name="database-connection" />
                    <label for="database-connection">Database Connection</label>
                </li>
            </ul>
            <div class="method-area">
                <!-- ==== STEP 2.1 - Bulk ===-->
                <Bulk v-if="currentStep === 2" ref="bulkRef" :goto-step="gotoStep" :selectedMethod="selectedMethod"
                    :createdDatasetId="createdDatasetId ? createdDatasetId : datasetId" :domains="domains"
                    @update:selectedMethod="emitMethodChange" />
                <!-- ==== STEP 2.2 - Streaming ===-->
                <Streaming ref="streamingRef" :currentStep="currentStep" :datasetName="datasetName"
                    :selectedMethod="selectedMethod" :createdDatasetId="createdDatasetId ? createdDatasetId : datasetId" :nextStep="nextStep"
                    @update:selectedMethod="emitMethodChange"
                    :showFeedback="showFeedback" :feedbackMessage="feedbackMessage" @updateFeedback="updateFeedback" />
                <!-- ==== STEP 2.3 - Database Connection ===-->
                <Database ref="databaseRef" :currentStep="currentStep" :datasetName="datasetName"
                    :selectedMethod="selectedMethod" :createdDatasetId="createdDatasetId ? createdDatasetId : datasetId" :nextStep="nextStep"
                    @update:selectedMethod="emitMethodChange" :submitDataStreaming="submitDataStreamingFunction"
                     />
            </div>
        </div>
    </form>
    <ClientOnly>
        <Teleport to="#modals-container">
            <ModalsModalFeedback v-if="showFeedback" :feedback="feedbackMessage" @toggle="close"/>
        </Teleport>
    </ClientOnly>
</template>

<script setup lang="ts">

import type { PropType, ComponentPublicInstance } from 'vue';
import type { Domain } from '~/interfaces/types';
import Bulk from './Bulk.vue';
import Streaming from './Streaming.vue';
import Database from './Database.vue';

// Define props that the component receives from its parent
const props = defineProps({
    datasetId: String,  // The unique identifier for the dataset, if it exists.
    datasetName: String,  // The name of the dataset being created or modified.
    currentStep: Number,  // The current step in the multi-step form process.
    selectedMethod: String,  // The currently selected method for data submission (e.g., Bulk, Streaming, Database).
    createdDatasetId: String,  // The unique identifier for the created dataset, used to track it after submission.
    gotoStep: {
        type: Function,  // Function to navigate between steps in the process.
        required: true,  // This function is required to be passed from the parent.
    },
    nextStep: {
        type: Function,  // Function to move to the next step in the process.
        required: true,  // This function is required to proceed to the next step.
    },
    domains: {
        type: Array as PropType<Domain[]>,  // List of domains or platforms available for selection.
        required: true,  // This array is required and used within the component.
    }
});

console.log('createdDatasetId', props.createdDatasetId, props.datasetId);

// Emits events to update the parent component
const emit = defineEmits(['update:selectedMethod']);

/**
 * Emits an event to update the parent component's selectedMethod property.
 * This is called when the user changes the selected method for data submission.
 * @param {string} method - The new method that the user has selected.
 */
const emitMethodChange = (method: string) => {
    emit('update:selectedMethod', method);
};

// Define the type for the Bulk component instance, which includes a method handleSubmitBulk
type BulkInstance = ComponentPublicInstance<{}, { handleSubmitBulk: () => void }>;

// Create a ref to hold the Bulk component instance
const bulkRef = ref<BulkInstance | null>(null); // Reference for Bulk component

const feedbackMessage = ref({ type: '', message: '' });
const showFeedback = ref(false);

const updateFeedback = (type: string, message: string) => {
  feedbackMessage.value = { type, message };
  showFeedback.value = true;
  setTimeout(() => {
    feedbackMessage.value = { type: '', message: '' };
    showFeedback.value = false;
  }, 3000);
};

const close = () => {
    showFeedback.value = false;
};

/**
 * Calls the handleSubmitBulk method on the Bulk component instance
 * to process the bulk file upload.
 */
const handleSubmitBulk = () => {
    bulkRef.value?.handleSubmitBulk();
};

// Define the type for the Streaming component instance, including methods for submitting data
type StreamingInstance = ComponentPublicInstance<{}, { handleSubmitStreaming: () => void; submitDataStreaming: (data: any) => Promise<void>; }>;
const streamingRef = ref<StreamingInstance | null>(null);

/**
 * Calls the handleSubmitStreaming method on the Streaming component instance
 * to process the data submission via the streaming method.
 */
const handleSubmitStreaming = () => {
    // Call Bulk's handleSubmitBulk method
    streamingRef.value?.handleSubmitStreaming();
};

// Computed property to get the submitDataStreaming function from the Streaming component
const submitDataStreamingFunction = computed(() => {
    return streamingRef.value?.submitDataStreaming || (() => Promise.resolve()); // Fallback to a resolved promise
});

// Define the type for the Database component instance, which includes a method handleSubmitDatabase
type DatabaseInstance = ComponentPublicInstance<{}, { handleSubmitDatabase: () => void }>;
const databaseRef = ref<DatabaseInstance | null>(null);

/**
 * Calls the handleSubmitDatabase method on the Database component instance
 * to process the data submission via the database method.
 */
const handleSubmitDatabase = () => {
    // Call Bulk's handleSubmitBulk method
    databaseRef.value?.handleSubmitDatabase();
};

// Expose methods so they can be called from outside the component
defineExpose({
    handleSubmitBulk,
    handleSubmitStreaming,
    handleSubmitDatabase
});
</script>