<!--
  ~ Copyright (c) 2024 LOBA
  ~ 
  ~ Permission is hereby granted, free of charge, to any person obtaining a copy of
  ~ this software and associated documentation files (the "Software"), to deal in
  ~ the Software without restriction, including without limitation the rights to
  ~ use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  ~ the Software, and to permit persons to whom the Software is furnished to do so,
  ~ subject to the following conditions:
  ~ 
  ~ The above copyright notice and this permission notice shall be included in all
  ~ copies or substantial portions of the Software.
  ~ 
  ~ THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  ~ IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  ~ FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  ~ COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  ~ IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  ~ CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  ~ 
  ~ SPDX-License-Identifier: MIT
  ~ 
  ~ Contributors:
  ~     [name] - [contribution]
-->

<template>
    <div class="modal">
        <div class="modal-mask"></div>
        <button class="close-btn button-icon" @click="closeModal();">
            <i class="icon-close"></i>
        </button>
        <div class="modal-wrapper">
            <div class="modal-sidebar">
                <div>
                    <h2>Create <br>New <br>{{ pageTitle }}</h2>
                    <div>
                        <nav>
                            <ul v-if="!datasetId">
                                <li :class="{ 'active': currentStep === 1 }"><div><span>1</span>Dataset Info</div></li>
                                <li :class="{ 'active': currentStep === 2 }"><div><span>2</span>Method</div></li>
                                <li :class="{ 'active': currentStep === 3 }"><div><span>3</span>Permissions</div></li>
                                <li :class="{ 'active': currentStep === 4 }"><div><span>4</span>Confirmation</div></li>
                            </ul>

                            <ul v-if="datasetId">
                                <li :class="{ 'active': currentStep === 2 }"><div><span>1</span>Method</div></li>
                                <li :class="{ 'active': currentStep === 3 }"><div><span>2</span>Permissions</div></li>
                            </ul>

                        </nav>
                    </div>
                </div>
                <div>
                    <div class="info user">
                        <h3>Owner</h3>
                        <UserProfile />
                    </div>
                    <InfoBox title="Creation date" text="17:14:12 <br> 19 Nov 2024" />
                </div>
            </div>
            <div class="modal-content">
                <!-- ==== STEP 1 - Dataset Info ====  -->
                <div id="step-1" class="step" v-show="currentStep === 1" v-if="!datasetId">
                    <Step1 ref="step1Ref" 
                        :currentStep="currentStep" 
                        :next-step="nextStep" 
                        :createdDatasetId="createdDatasetId" 
                        :domains="domains" 
                        :vocabularies="vocabularies"
                        :isEditMode="isEditMode"
                        :id="id"
                        @datasetCreated="handleDatasetCreated"
                        @updateData="handleUpdateData"  />
                </div>

                <!-- ==== STEP 2 - Method ===-->
                <div id="step-2" class="step" v-show="currentStep === 2">
                    <Step2 ref="step2Ref" 
                        :datasetId="datasetId" 
                        :goto-step="gotoStep" 
                        :next-step="nextStep" 
                        :currentStep="currentStep" 
                        :domains="domains" 
                        :createdDatasetId="createdDatasetId" 
                        :datasetName="datasetName"
                        v-model:selectedMethod="selectedMethod" 
                        @datasetCreated="handleDatasetCreated" />
                </div>

                <!-- ==== STEP 3 - Permissions === -->
                <div id="step-3" class="step" v-show="currentStep === 3">
                    <Step3 :datasetId="datasetId"/>
                </div>

                <!-- ==== STEP 4 - Confirmation === -->
                <div id="step-4" class="step" v-show="currentStep === 4">
                    <Step4 :datasetId="datasetId" :close-modal="closeModal" :checkedDomainsLength="checkedDomainsLength" :selectedTermsLength="selectedTermsLength" :pageTitle="pageTitle" :selectedMethod="selectedMethod" :datasetName="datasetName"  />
                </div>

            </div>
            <div class="modal-buttons-area">
                <button :class="['button-stroke-transparent rounded', currentStep === 1 && 'hide' , (currentStep === 2 && datasetId) && 'hide']" id="previous-step"
                    @click="previousStep">Back</button>
                <button class="button-secondary orange" id="next-step" @click="handleNextClick"
                    v-if="currentStep !== 4">{{ buttonText
                    }}</button>
                <div id="loading-spinner" class="hidden">
                    <div class="spinner"></div>
                </div>
            </div>
        </div>

    </div>
</template>
<script setup lang="ts">

import Id from '~/pages/catalog/[id].vue';
import Step1 from '../datasetSteps/Step1.vue';
import Step2 from '../datasetSteps/Step2.vue';
import Step3 from '../datasetSteps/Step3.vue';
import Step4 from '../datasetSteps/Step4.vue';
import type { vocabulariesArray, Domain } from '~/interfaces/types';
import { useDataStore } from '@/stores/dataStore';

// Define an event emitter for the "toggle" event, used to close the modal
const emit = defineEmits(['toggle', 'refresh-data']);
const dataStore = useDataStore();

/**
 * Emit the "toggle" event to close the modal.
 */
const closeModal = () => {
    emit('toggle');
    emit('refresh-data');
    dataStore.triggerRefresh();
}

// Define the component's props, specifically allowing for an optional datasetId
const props = defineProps<{
    datasetId?: string;
    isEditMode?: boolean;
    id?: string;
}>();

// Define reactive state variables
const buttonText = ref('Create');
const selectedMethod = ref('bulk');

// Interfaces to define component methods that will be referenced
export interface Step1Component {
  handleSubmit: () => void;
}

export interface Step2Component {
    handleSubmitBulk: () => void;      // Submit handler for "Bulk" method in Step 2
    handleSubmitStreaming: () => void; // Submit handler for "Streaming" method in Step 2
    handleSubmitDatabase: () => void;  // Submit handler for "Database" method in Step 2
}

// Create references for Step1 and Step2 components to trigger form submissions
const step1Ref = ref<Step1Component | null>(null);
const step2Ref = ref<Step2Component | null>(null);

// Define available domains in the form as a reactive array
const domains = ref<Domain[]>([
    {
        name: 'Mathematics'
    },
    {
        name: 'Computer Sciente / Finance'
    },
    {
        name: 'Medicine / Health Sciences'
    },
    {
        name: 'Civil Engineering'
    },
    {
        name: 'Computer Science / Artificial Intelligence'
    }
])

// Define available vocabularies and terms in the form as a reactive array
const vocabularies = ref<vocabulariesArray[]>([
    {
        id: 1,
        name: 'Mathematics',
        total: 31,
        terms: [
            {
                id: 1,
                name: 'Blockchain Tecnology',
            },
            {
                id: 2,
                name: 'Blockchain Tecnology',
            },
            {
                id: 3,
                name: 'Blockchain Tecnology',
            },
            {
                id: 4,
                name: 'Blockchain Tecnology',
            },
            {
                id: 5,
                name: 'Blockchain Tecnology',
            },
            {
                id: 6,
                name: 'Blockchain Tecnology',
            },
            {
                id: 7,
                name: 'Blockchain Tecnology',
            },
            {
                id: 8,
                name: 'Blockchain Tecnology',
            },
            {
                id: 9,
                name: 'Blockchain Tecnology',
            },
            {
                id: 10,
                name: 'Blockchain Tecnology',
            }
        ]
    },
    {
        id: 2,
        name: 'Computer Sciente / Finance',
        total: 29,
        terms: [
            {
                id: 1,
                name: 'Blockchain Tecnology',
            },
            {
                id: 2,
                name: 'Blockchain Tecnology',
            },
            {
                id: 3,
                name: 'Blockchain Tecnology',
            },
            {
                id: 4,
                name: 'Blockchain Tecnology',
            },
            {
                id: 5,
                name: 'Blockchain Tecnology',
            },
            {
                id: 6,
                name: 'Blockchain Tecnology',
            },
            {
                id: 7,
                name: 'Blockchain Tecnology',
            },
            {
                id: 8,
                name: 'Blockchain Tecnology',
            },
            {
                id: 9,
                name: 'Blockchain Tecnology',
            },
            {
                id: 10,
                name: 'Blockchain Tecnology',
            }
        ]
    },
    {
        id: 3,
        name: 'Medicine / Health Sciences',
        total: 0,
        terms: [
            {
                id: 1,
                name: 'Other',
            },

        ]
    }
])

/**
 * Fetch available domains and vocabularies on API upon component mounting.
 */
onMounted(() => {
    // get domains
    useFetchAuth('/domains').then((response: any) => {
        domains.value = [];
        for (let i = 0; i < response.length; i++) {
            domains.value.push({
                name: response[i].name
            });
        }
    });

    // get vocabularies + terms
    useFetchAuth('/glossaries').then((response: any) => {
        vocabularies.value = [];
        for (let i = 0; i < response.length; i++) {

            // get terms

            vocabularies.value.push({
                id: response[i].id,
                name: response[i].name,
                total: response[i].terms.length,
                terms: response[i].terms
            });

        }
    });
});

// Dataset-related information after dataset creation
const createdDatasetId = ref<string>('');
const datasetName = ref<string>('');

const checkedDomainsLength = ref(0);
const selectedTermsLength = ref(0);

const handleUpdateData = (data: { checkedDomainsLength: number; selectedTermsLength: number }) => {
  checkedDomainsLength.value = data.checkedDomainsLength;
  selectedTermsLength.value = data.selectedTermsLength;
};

/**
 * Handle dataset creation event from a child component.
 * 
 * @param data - Object containing the createdDatasetId and datasetName
 */
const handleDatasetCreated = (data: { createdDatasetId: string, datasetName: string }) => {
    createdDatasetId.value = data.createdDatasetId;
    datasetName.value = data.datasetName;
};

// Modal navigation state tracking
const currentStep = ref<number>(1);
// Control whether the "Back" button is hidden (e.g., when a dataset is already provided)
const hideBack = ref<boolean>(false);

/**
 * Set initial navigation state if a datasetId is passed as a prop.
 */
onMounted(() => {
    if (props.datasetId) {
        hideBack.value = true
        currentStep.value = 2
    }
})

// Computed property for dynamic title
const pageTitle = computed(() => {
    return props.datasetId ? 'Artifact' : 'Dataset';
});
/**
 * Handle the "Next" button click based on the current step.
 * - Step 1: Triggers form submission in Step1 component.
 * - Step 2: Triggers specific submission method based on the selectedMethod (bulk/streaming/database).
 */
const handleNextClick = () => {
    if (currentStep.value === 1) {
        // Trigger form submission and validation only on step 1
        step1Ref.value?.handleSubmit();
    } else if (currentStep.value === 2) {

        // Trigger form submission and validation only on step 2 streaming
        if (selectedMethod.value === 'bulk') {
            step2Ref.value?.handleSubmitBulk();
        } else if (selectedMethod.value === 'streaming') {
            step2Ref.value?.handleSubmitStreaming();
        } else if (selectedMethod.value === 'database') {
            step2Ref.value?.handleSubmitDatabase();
        }
    }else if(currentStep.value === 3){
        if(props.datasetId){
            closeModal();
        }else{
            nextStep();
        }
    } else {
        // Proceed to the next step without form submission
        nextStep();
    }
}

/**
 * Proceed to the next step in the form wizard.
 */
const nextStep = () => {
    if (currentStep.value < 4) {
        currentStep.value++;
    }
}

/**
 * Go back to the previous step in the form wizard.
 */
const previousStep = () => {
    if (currentStep.value > 0) {
        currentStep.value--;
    }
}

/**
 * Go to a specific step in the form wizard, and update the button text accordingly.
 * @param {number} step The step number to go to.
 */
const gotoStep = (step: number) => {
    currentStep.value = step;

    if (currentStep.value === 1 || currentStep.value === 2) {
        buttonText.value = 'Create';
    }else if(currentStep.value === 3){
        if(props.datasetId){
            buttonText.value = 'Apply Permissions and Close';
        }else{
            buttonText.value = 'Apply Permissions';
        }
    } else {
        buttonText.value = 'Apply Permissions';
    }
}


</script>