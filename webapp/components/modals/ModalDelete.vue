<!--
  ~ Copyright (c) 2024 LOBA
  ~ 
  ~ Permission is hereby granted, free of charge, to any person obtaining a copy of
  ~ this software and associated documentation files (the "Software"), to deal in
  ~ the Software without restriction, including without limitation the rights to
  ~ use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  ~ the Software, and to permit persons to whom the Software is furnished to do so,
  ~ subject to the following conditions:
  ~ 
  ~ The above copyright notice and this permission notice shall be included in all
  ~ copies or substantial portions of the Software.
  ~ 
  ~ THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  ~ IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  ~ FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  ~ COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  ~ IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  ~ CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  ~ 
  ~ SPDX-License-Identifier: MIT
  ~ 
  ~ Contributors:
  ~     [name] - [contribution]
-->

<template>
    <div class="modal mini-modal">
        <div class="modal-success" v-if="showSuccessDelete">
            <div class="d-flex"> 
                <div class="success-area">
                <div class="success-icon"><i class="icon-check"></i></div>
                    <div class="feedback-message">
                        <p>{{ type }} deleted successfully!</p>
                    </div>
                </div>
            </div>
        </div>
        <div v-else class="modal-message">
            <div class="d-flex"> 
                <div>
                    <i class="warning"></i>
                </div>
                <div>
                    <h2>Are you sure you want to eliminate this {{ type }}?</h2>
                    <p><b>Attention:</b> By clicking “Delete anyway”, you are deleting this {{ type }} from the platform and all Related Entities will lose this relation.</p>
                    <div class="error-block" v-if="showErrorDelete">
                        <div class="error-icon"><i class="icon-close"></i></div>
                        <div class="error-title"><span>Error: </span> Unable to delete item</div>
                        <div class="error-text">{{ errorMessage }}</div>
                    </div>
                    <div class="buttons-wrapper">
                        <button class="button-secondary orange rounded" @click="deleteItem" v-if="!showErrorDelete">Delete anyway</button>
                        <button class="button-stroke-transparent rounded" @click="closeModal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</template>
<script setup lang="ts">
import { useDataStore } from '@/stores/dataStore';

const dataStore = useDataStore();

const props = defineProps({
    value:{
        type: String,
        required: false
    },
    type: { 
        type: String, 
        required: false 
    },
    parentId: {
        type: String,
        required: false
    }
});

const router = useRouter();

const emit = defineEmits(['toggle']);

const closeModal = () => {
    emit('toggle');
    dataStore.triggerRefresh();
}

const showErrorDelete = ref(false);
const showSuccessDelete = ref(false);
const errorMessage = ref('');

const deleteItem = () => {
    if(props?.type === 'Domain') {
        deleteDomain();
    }else if(props?.type === 'Glossary') {
        deleteGlossary();
    }else if(props?.type === 'Dataset') {
        deleteDataset();
    }else if(props?.type === 'Term') {
        deleteTerm();
    }
}

const deleteDomain = () => {
    try {

        useFetchAuth('/domains/' + props?.value, { 
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            },
        }).then((response: any) => {
            showErrorDelete.value = false;
            showSuccessDelete.value = true;
            setTimeout(() => {
                navigateTo('/domains');
                closeModal();
                showSuccessDelete.value = false;
            }, 3000);
            
        }).catch((error) => {
            console.log('domain error 1', error.response._data.message)
            const errorData = error.response._data.message || error; // Access the response data if available 
            showErrorDelete.value = true;
            showSuccessDelete.value = false;
            errorMessage.value = errorData || 'An unexpected error occurred.';
            
            return;
        });
    } catch (error) {
        console.error('Error during fetch:', error);
        errorMessage.value = 'Failed to communicate with the server. Please try again later.';
        showErrorDelete.value = true;
    }
}

const deleteGlossary = async() => {
    try {
        const response = await useFetchAuth('/glossaries/' + props?.value, { 
            method: 'DELETE',
            headers: {
            'Content-Type': 'application/json'
            },
        }).then((response: any) => {
            console.log('Data deleted successfully');
            showErrorDelete.value = false;
            showSuccessDelete.value = true;
            setTimeout(() => {
                closeModal();
                navigateTo('/glossary');
                showSuccessDelete.value = false;
            }, 3000);
            
            
        }).catch((error) => {
            const errorData = error.response._data.message || error;
            showErrorDelete.value = true;
            showSuccessDelete.value = false;
            errorMessage.value = errorData?.message || 'An unexpected error occurred.';
            return;
        }); 
    } catch (error) {
        console.error('Error during fetch:', error);
        errorMessage.value = 'Failed to communicate with the server. Please try again later.';
        showErrorDelete.value = true;
    }
}

const deleteDataset = async() => {
    try {
        const responseOld = await useFetchAuth(`/catalogs/${props?.value}`) as { domains: any, terms: any };
        
        if (responseOld?.domains?.length) {
            await Promise.all(
                responseOld.domains.map((domain: any) => removeAssociatedDomains(domain.name, props?.value))
            );
        }

        if (responseOld?.terms?.length) {
            await Promise.all(
                responseOld.terms.map((term: any) => removeAssociatedTermsEntities(term.id, term.relationId))
            );
        }

        useFetchAuth('/catalogs/' + props?.value, { 
            method: 'DELETE',
            headers: {
            'Content-Type': 'application/json'
            },
        }).then((response: any) => {
            console.log('Data deleted successfully');
            showErrorDelete.value = false;
            showSuccessDelete.value = true;
            setTimeout(() => {
                closeModal();
                navigateTo('/catalog');
                showSuccessDelete.value = false;
            }, 3000);
        }).catch((error) => {
            const errorData = error.response._data.message || error;
            showErrorDelete.value = true;
            showSuccessDelete.value = false;
            errorMessage.value = errorData?.message || 'An unexpected error occurred.';
            return;
        });
    } catch (error) { 
        showErrorDelete.value = true;
        errorMessage.value = (error as Error)?.message || 'An unexpected error occurred.';
        console.error('Error during fetch:', error);
    }
}

const deleteTerm = async() => {
    try {
        // delete terms relations
        if(props?.value){
            const responseOld = await useFetchAuth(`/terms/${props?.value}`) as { synonyms: any, antonyms: any };

            if(responseOld?.synonyms?.length){
                await Promise.all(
                    responseOld.synonyms.map((synonym: any) => removeAssociatedTerms(synonym?.relationId))
                )
            }

            if(responseOld?.antonyms?.length){
                await Promise.all(
                    responseOld.antonyms.map((antonym: any) => removeAssociatedTerms(antonym?.relationId))
                )
            }
        }

        useFetchAuth('/terms/' + props?.value, { 
            method: 'DELETE',
            headers: {
            'Content-Type': 'application/json'
            },
        }).then((response: any) => {
            console.log('Data deleted successfully');
            showErrorDelete.value = false; // Hide the error block if previously visible
            showSuccessDelete.value = true;
            setTimeout(() => {
                navigateTo('/glossary/' + props?.parentId);
                closeModal();
                showSuccessDelete.value = false;
            }, 3000);
            
        }).catch((error) => {
            const errorData = error.response._data.message || error;// Parse the error response body if available
            errorMessage.value = errorData?.message || 'An unexpected error occurred.';
            showErrorDelete.value = true; // Show the error block
            showSuccessDelete.value = false;
        });
    } catch (error) {
        console.error('Error during fetch:', error);
        errorMessage.value = 'Failed to communicate with the server. Please try again later.';
        showErrorDelete.value = true;
    }
}

</script>
