<!--
  ~ Copyright (c) 2024 LOBA
  ~ 
  ~ Permission is hereby granted, free of charge, to any person obtaining a copy of
  ~ this software and associated documentation files (the "Software"), to deal in
  ~ the Software without restriction, including without limitation the rights to
  ~ use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  ~ the Software, and to permit persons to whom the Software is furnished to do so,
  ~ subject to the following conditions:
  ~ 
  ~ The above copyright notice and this permission notice shall be included in all
  ~ copies or substantial portions of the Software.
  ~ 
  ~ THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  ~ IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  ~ FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  ~ COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  ~ IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  ~ CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  ~ 
  ~ SPDX-License-Identifier: MIT
  ~ 
  ~ Contributors:
  ~     [name] - [contribution]
-->

<template>
    <div class="dashboard-title">
        <h2>Last Data Products added</h2>
        <ul class="nav nav-pills" id="pills-tab" role="tablist">
            <li v-for="tab1 in tabs" :key="tab1.id" :class="['nav-item', { 'active': selectedTab === tab1.id }]"
                @click="selectTab(tab1.id)" role="presentation">
                <button class="nav-link" :id="`pills-${tab1.value}-tab`" data-bs-toggle="pill"
                    :data-bs-target="`#pills-${tab1.value}`" type="button" role="tab"
                    :aria-controls="`pills-${tab1.value}`" aria-selected="true">{{ tab1.name }}</button>
            </li>
        </ul>
        <button @click="goCatalog" class="button-default purple">
            <span class="button-inner "><span><i class="icon-arrow-right"></i></span>Explore Catalog</span>
        </button>
    </div>
    <div class="tab-content" id="pills-tabContent">
        <template v-for="(tab, tabIndex) in tabs" :key="tab.id">
            <div :id="`pills-${tab.value}`" v-if="selectedTab === tab.id" role="tabpanel"
                :aria-labelledby="`pills-${tab.value}-tab`">
                <Datatable :data="tab.data"></Datatable>
            </div>
        </template>
    </div>
</template>

<script setup lang="ts">
import { ref, type Ref } from 'vue';
import type DataTableI from '~/interfaces/datatable';
import { formatDate } from '~/utils/utils';

interface Dataset {
    id: number;
    name: string;
    type: string;
    platform: string;
    domain: string;
    quality: number;
    owner: number;
}

interface DatasetDetails {
    domains: any;
    description: string;
    terms: any;
    owner: string;
    ownerName: string;
    createdAt: string;
    updatedAt: string;
}

useFetchAuth('/search', {
    method: 'POST',
    body: { type: 'CATALOG', limit: 25, offset: 0 }
})
    .then(async (response: any) => {
        const datasets: Dataset[] = response;

        // Map the response data to match the structure of `rows`
        const rows = datasets.map((dataset: Dataset, index: number) => ({
            id: dataset.id,
            name: dataset.name,
            type: dataset.type,
            platform: 'AWS S3',
            domain: dataset.domain,
            quality: 97,
            edited_at: '',
            actions: ''
        }));

        const detailedRows = await Promise.all(
            datasets.map(async (dataset: Dataset) => {
                try {
                    const details = await useFetchAuth(`/catalogs/${dataset.id}`) as DatasetDetails;

                    return {
                        id: dataset?.id,
                        name: dataset?.name,
                        type: dataset?.type,
                        platform: 'AWS S3',
                        domain: details?.domains?.join(', '),
                        quality: 97,
                        owner: details?.owner,
                        ownerName: details?.ownerName,
                        edited_at: formatDate(details?.updatedAt),
                        created_at: formatDate(details?.createdAt),
                        actions: '',
                        description: details?.description,
                        terms: details?.terms,

                    };
                } catch (error) {
                    console.error(`Failed to fetch details for dataset ${dataset.id}:`, error);
                    return {
                        id: dataset?.id,
                        name: dataset?.name,
                        type: dataset?.type,
                        platform: 'AWS S3',
                        domain: '',
                        quality: 97,
                        owner: '',
                        ownerName: '',
                        edited_at: '',
                        created_at: '',
                        actions: '',
                        description: '',
                        terms: '',
                    };
                }
            })
        );

        // Update the rows in `lastDatasets`
        lastDatasets.value.rows = detailedRows;
    })
    .catch(error => {
        console.error('API Error:', error);
    });

const dataProducts = ref<DataTableI>({
    options: {
        tableName: 'catalog-table',
        detailsPath: '/catalog/',
        allowCheckRow: true,
        favourites: true,
        previewComponent: 'CatalogDetails',
    },
    columns: [
        {
            id: 1,
            name: 'name',
            label: 'Name',
            width: '270px',
            combineColumn: 'type',
            renderComponent: 'TableColumnsName'
        },
        {
            id: 2,
            name: 'platform',
            label: 'Platform',
            width: '100px'
        },
        {
            id: 3,
            name: 'domain',
            label: 'Domain',
            width: '160px'
        },
        {
            id: 4,
            name: 'quality',
            label: 'Quality',
            width: '60px',
            renderComponent: 'TableColumnsQuality'
        },
        {
            id: 5,
            name: 'owner',
            label: 'Owner',
            width: '150px',
            renderComponent: 'UserProfile'
        },
        {
            id: 6,
            name: 'edited_at',
            label: 'Edited at',
            width: '100px'
        },
        {
            id: 7,
            name: 'actions',
            label: 'Actions',
            width: '80px',
            hideLabel: true
        }
    ],
    rows: []
});

const lastDatasets = ref<DataTableI>({
    options: {
        tableName: 'catalog-table',
        detailsPath: '/catalog/',
        favourites: true,
        previewComponent: 'CatalogDetails',
    },
    columns: [
        {
            id: 1,
            name: 'name',
            label: 'Name',
            width: '300px',
            combineColumn: 'type',
            renderComponent: 'TableColumnsName'
        },
        {
            id: 2,
            name: 'platform',
            label: 'Platform',
            width: '100px'
        },
        {
            id: 3,
            name: 'domain',
            label: 'Domain',
            width: '140px'
        },
        {
            id: 3,
            name: 'quality',
            label: 'Quality',
            width: '100px',
            renderComponent: 'TableColumnsQuality'
        },
        {
            id: 4,
            name: 'owner',
            label: 'Owner',
            width: '200px',
            renderComponent: 'UserProfile'
        },
        {
            id: 5,
            name: 'edited_at',
            label: 'Edited at',
            width: '70px'
        },
        {
            id: 6,
            name: 'actions',
            label: 'Actions',
            width: '80px',
            hideLabel: true
        }
    ],
    rows: []
});

const tabs = [{
    id: 1,
    name: 'All Catalog',
    value: 'all',
    data: lastDatasets.value
},
{
    id: 2,
    name: 'Data Products',
    value: 'data_products',
    data: dataProducts.value
},
{
    id: 3,
    name: 'Datasets',
    value: 'datasets',
    data: lastDatasets.value
}
];

const selectedTab = ref(tabs[0].id);

function selectTab(tabId: number) {
    selectedTab.value = tabId;
}


const goCatalog = () => {
    console.log('asdsa');
    navigateTo('/catalog');
}

</script>