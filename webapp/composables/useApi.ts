/**
 * Copyright (c) 2024 LOBA
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * SPDX-License-Identifier: MIT
 * 
 * Contributors:
 *     [name] - [contribution]
 */

import { useFetch } from "nuxt/app";
type useFetchType = typeof useFetch;

export const useFetchAPI: useFetchType = (path, options = {}) => {
    const token = localStorage.getItem('dm_atoken') || '';

    const baseURL = useRuntimeConfig().public.API_BASE_URL;

    if (!baseURL) {
        throw new Error("API Base URL is not defined!");
    }

    // Append the lang parameter if needed
    // const lang = useI18n().locale.value;
    // path = path.includes('?') ? `${path}&lang=${lang}` : `${path}?lang=${lang}`;

    options.baseURL = baseURL;
    options.headers = {
        ...options.headers,
        Authorization: `Bearer ${token}`,
    };

    console.log('Full URL:', `${options.baseURL}${path}`);
    console.log('Fetch options:', options);

    return useFetch(path, options);
}
