/**
 * Copyright (c) 2024 LOBA
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * SPDX-License-Identifier: MIT
 * 
 * Contributors:
 *     [name] - [contribution]
 */

export default defineNuxtRouteMiddleware((to, from) => {

    // if AUTH_ENABLE in env is true
    if(!process.env.AUTH_ENABLE) return;

    // routes to ignore this middleware
    const unprotectedRoutes = ['login','auth-comeback'];
    // console.log(to.name);return;
    if(unprotectedRoutes.includes(to.name as string)) return;

    // check if has tokens
    if(!window.localStorage.getItem('dm_atoken')) return navigateTo('/login');
    if(!window.localStorage.getItem('dm_rtoken')) return navigateTo('/login');
    if(!window.localStorage.getItem('dm_etoken')) return navigateTo('/login');

    // validate token expire date - add 5 minutes
    const threshold = 1000 * 60 * 5;
    if(Date.now() <= Number(window.localStorage.getItem('dm_etoken')) - threshold ) {
        console.log('time to refresh the token');
    }

});