/**
 * Copyright (c) 2024 LOBA
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * SPDX-License-Identifier: MIT
 * 
 * Contributors:
 *     [name] - [contribution]
 */

// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
	ssr: false,
	devtools: { enabled: true },
	devServer: {
		port: 3000,
		host: '0.0.0.0'
	},
	modules: [
		'@pinia/nuxt',
		'@nuxtjs/i18n',
		'nuxt-swiper'
	],
	// i18n: {
	// 	lazy: true,
	// 	langDir: 'locales/',
	// 	locales: [
	// 		{
	// 			code: 'en',
	// 			file: 'load-locale.ts'
	// 		},
	// 		{
	// 			code: 'fr',
	// 			file: 'load-locale.ts'
	// 		}
	// 	],
	// 	strategy: 'no_prefix',
	// 	defaultLocale: 'en',
	// },
	app: {
		head: {
			title: 'DATAMITE',
			charset: 'utf-8',
			viewport: 'width=device-width, initial-scale=1',
			link: [
				{ rel: "icon", type: "image/png", href: "/favicon.png" }
			],
			htmlAttrs: {
				lang: 'en'
			}
		}
	},
	runtimeConfig: {
		public: {
			AUTH_ENABLE: process.env.AUTH_ENABLE,
			API_BASE_URL: process.env.API_BASE_URL,
			API_DATAMITE_URL: process.env.API_DATAMITE_URL,
			API_DATAMITE_URL_V2: process.env.API_DATAMITE_URL_V2,
			API_DATAMITE_STREAMING_SERVICE: process.env.API_DATAMITE_STREAMING_SERVICE,
			API_DATAMITE_DATA_STORAGE: process.env.API_DATAMITE_DATA_STORAGE,
			API_DATAMITE_QUALITY_RULES: process.env.API_DATAMITE_QUALITY_RULES,
		},
	},
	plugins: [
		{ src: '~/plugins/bootstrap.client.ts', mode: 'client' }
	],
	telemetry: false
})