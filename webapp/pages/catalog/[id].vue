<!--
  ~ Copyright (c) 2024 LOBA
  ~ 
  ~ Permission is hereby granted, free of charge, to any person obtaining a copy of
  ~ this software and associated documentation files (the "Software"), to deal in
  ~ the Software without restriction, including without limitation the rights to
  ~ use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  ~ the Software, and to permit persons to whom the Software is furnished to do so,
  ~ subject to the following conditions:
  ~ 
  ~ The above copyright notice and this permission notice shall be included in all
  ~ copies or substantial portions of the Software.
  ~ 
  ~ THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  ~ IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  ~ FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  ~ COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  ~ IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  ~ CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  ~ 
  ~ SPDX-License-Identifier: MIT
  ~ 
  ~ Contributors:
  ~     [name] - [contribution]
-->

<template>
	<NuxtLayout name="default">
		<div v-if="datasetDetails">

			<!-- sidebar navigation teleported to template -->
			<SidebarNav :nav="navItems" :title="'Catalog'" />

			<TopbarTopBar :breadcrumb="breadcrumb" @edit-click="openEditModal" @delete-click="openDeleteModal" />
			<div class="detail-header">
				<div class="row">
					<div class="col-xl-4">
						<div class="title">
							<h2>{{ datasetDetails?.name }}</h2>
							<Badge />
						</div>
					</div>
				</div>
			</div>
			<div class="detail-body">
				<div class="row">
					<div class="col-xl-3">
						<div class="about">
							<InfoBox title="Creation date" :text="formatDate(datasetDetails?.createdAt) || ''" />
							<InfoBox title="Last Edition" :text="formatDate(datasetDetails?.updatedAt) || ''" />
						</div>
						<div class="info user">
							<h3>Owner</h3>
							<UserProfile />
						</div>
						<div class="info user">
							<h3>Shared with</h3>
							<UsersSelectionList />
						</div>
					</div>
					<div class="col-xl-8 offset-xl-1">
						<div class="about details">
							<InfoBox title="Domains" :text="formattedDomains ? formattedDomains : '-'" />
							<!-- <InfoBox title="File Format" text=".csv" />
							<InfoBox title="Platform" text="AWS S3" /> -->
							<!-- <InfoBox title="Quality" text="" /> -->
						</div>
						<div class="row">
							<div class="col-xl-9">
								<EditableTextBox title="Full description" :text="datasetDetails?.description || '-'" />
								<EditableTextBox title="Short description" :text="datasetDetails?.summary || '-'" />
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="detail-tab">
				<CatalogDetailTabs :data="datasetDetails" :key="datasetDetails?.id" />
			</div>
			<div class="offset-xl-1 col-xl-10 mt-160">
				<BannersBannerCta :banners="BannerCta" />
			</div>
		</div>
		
		<ClientOnly>
            <Teleport to="#modals-container">
                <ModalsCreateDataset v-if="showCreateDatasetModal" @toggle="close" :isEditMode="isEditMode" :id="route.params.id.toString()" @refresh-data="fetchData" />
				<ModalsModalDelete v-if="showDeleteModal" @toggle="closeDelete" :value="route.params.id.toString()" type="Dataset" />
			</Teleport>
        </ClientOnly>

	</NuxtLayout>
</template>
<script setup lang="ts">
import CatalogDetailTabs from '~/components/tabs/CatalogDetailTabs.vue';
import type NavItemI from '~/interfaces/navItem';
import slide1 from '@/assets/images/slide1.png';
import type sliderItemI from '~/interfaces/sliderItem';
import { ref, onMounted, watch } from 'vue';
import { useRoute } from 'vue-router';
import { formatDate } from '~/utils/utils';
import { storeToRefs } from 'pinia';
import { useDataStore } from '@/stores/dataStore';

const route = useRoute();
const dataStore = useDataStore();
const { refreshKey } = storeToRefs(dataStore); 

// Define Breadcrumb interface
interface BreadcrumbI {
    title: string;
    link: string;
}

/** CATALOG (Datasets)  - get dataset details **/
const datasetDetails = ref<any>(null);
const breadcrumb = ref<BreadcrumbI[]>([]);
const isEditMode = ref(false);
const showDeleteModal = ref(false);

const closeDelete = () => {
    showDeleteModal.value = false;
};

const openDeleteModal = () => {
	showDeleteModal.value = true;
};


const fetchData = async () => {
  if (route.params.id) {
    try {
      const response = await useFetchAuth(`/catalogs/${route.params.id}`);
      datasetDetails.value = response || {};
      breadcrumb.value = [
        { title: 'Catalog', link: '/catalog' },
        { title: datasetDetails.value?.name, link: '#' },
      ];
      console.log('Fetched data:', datasetDetails.value);
    } catch (error) {
      console.error('Error fetching data:', error);
      datasetDetails.value = {}; // Reset on error
    }
  }
};
onMounted(async () => {
	await fetchData();
});

watch(() => route.params.id, async() => {
	if (route.path.startsWith('/catalog/artifact/')) return;
	await fetchData();
}, { immediate: true });

watch(refreshKey, async(newKey) => {
	await fetchData();
});

/** CATALOG (Datasets) - get formatted domains related to dataset **/

const formattedDomains = computed(() => {
	const domains = datasetDetails.value?.domains || [];

	if (Array.isArray(domains)) {
		const domainNames = domains.map((domain) => domain.name);
		if (domainNames.length > 3) {
			const firstThree = domainNames.slice(0, 3).join(', ');
			const remainingCount = domainNames.length - 3;
			return `${firstThree}, +${remainingCount}`;
		} else {
			return domainNames.join(', ');
		}
	}
	return '';
});

const viewDefault = () => {
	console.log('show default home page');
}

const dummyClick = () => {
	console.log('dummyClick');
}

const navItems = ref<NavItemI[]>([
	{
		label: 'All',
		icon: 'list',
		action: viewDefault
	},
	{
		label: 'Combine Datasets',
		icon: 'chrome',
		action: dummyClick
	},
	{
		label: 'Recently Viewed',
		icon: 'clock',
		action: dummyClick
	},
	{
		label: 'Highlighted',
		icon: 'favourite',
		action: dummyClick
	},
	{
		label: 'Your Catalog',
		icon: 'loading',
		action: dummyClick
	}
]);


const BannerCta = ref<sliderItemI[]>([
	{
		id: 1,
		title: 'Have a question or doubts?',
		text: 'Check our FAQs and Documentation',
		button_text: 'Faqs and Documentation',
		button_link: 'https://www.google.com',
		media_type: 1,
		media_url: slide1,
		template: 1,
	}
]);

const showCreateDatasetModal = ref(false);

const close = () => {
    showCreateDatasetModal.value = false;
	isEditMode.value = false;
};

const openEditModal = () => {
	showCreateDatasetModal.value = true;
	isEditMode.value = true;
}
</script>