<!--
  ~ Copyright (c) 2024 LOBA
  ~ 
  ~ Permission is hereby granted, free of charge, to any person obtaining a copy of
  ~ this software and associated documentation files (the "Software"), to deal in
  ~ the Software without restriction, including without limitation the rights to
  ~ use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  ~ the Software, and to permit persons to whom the Software is furnished to do so,
  ~ subject to the following conditions:
  ~ 
  ~ The above copyright notice and this permission notice shall be included in all
  ~ copies or substantial portions of the Software.
  ~ 
  ~ THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  ~ IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  ~ FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  ~ COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  ~ IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  ~ CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  ~ 
  ~ SPDX-License-Identifier: MIT
  ~ 
  ~ Contributors:
  ~     [name] - [contribution]
-->

<template>
	<NuxtLayout name="default">
		<div>

			<!-- sidebar navigation teleported to template -->
			<SidebarNav :nav="navItems" :title="'Glossary'" />

			<TopbarTopBar :breadcrumb="breadcrumb" @edit-click="handleEditClick" @delete-click="openDeleteModal" />
			<div class="detail-header">
				<div class="row">
					<div class="col-xl-12">
						<div class="title">
							<h2>{{ glossaryDetails?.name }}</h2>
							<span class="type-glossary">Glossary</span>
						</div>
					</div>
				</div>
			</div>
			<div class="detail-body">
				<div class="row">
					<div class="col-xl-3">
						<div class="about">
							<InfoBox title="Creation date" :text="formatDate(glossaryDetails?.createdAt)" />
							<InfoBox title="Last Edition" :text="formatDate(glossaryDetails?.updatedAt)" />
						</div>
						<div class="info user">
							<h3>Owner</h3>
							<UserProfile />
						</div>
						<div class="info user">
							<h3>Shared with</h3>
							<UsersSelectionList />
						</div>
					</div>
					<div class="col-xl-8 offset-xl-1">
						<div class="row">
							<div class="col-xl-9" v-if="glossaryDetails">
								<EditableTextBox title="Full description" :text="glossaryDetails?.description" />
								<EditableTextBox title="Short description" :text="glossaryDetails?.summary" />
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="detail-tab"> 
				<TabsGlossaryDetailTabs :data="glossaryDetails" :key="glossaryDetails?.id" :terms="false" />
				<!-- <div class="glossary-tab-content">
					<div class="catalog-page">
						<Datatable :data="data" @open-edit-modal="handleOpenEditModal">
							<button class="button-stroke-transparent" @click="showCreateTermModal = true">Add
								Term</button>
							<div class="filters-top-totals"><b>{{ data.rows?.length }} Term(s)</b> available
							</div>
						</Datatable>
					</div>
				</div> -->
			</div>
			<div class="container">
				<div class="offset-xl-1 col-xl-10 mt-160">
					<BannersBannerCta :banners="BannerCta" />
				</div>
			</div>
			<ClientOnly>
				<Teleport to="#modals-container">
					<ModalsCreateTerm v-if="showCreateTermModal" @toggle="close" :glossaryId="glossaryDetails?.id" :isEditMode="isEditMode" :row="selectedRow ? selectedRow : ''"  />
					<ModalsCreateGlossary v-if="showCreateGlossaryModal" @toggle="close" :isEditMode="isEditMode" :row="glossaryDetails?.id" @refresh-data="fetchData" />
					<ModalsModalDelete v-if="showDeleteModal" @toggle="closeDelete" :value="glossaryDetails?.id" type="Glossary" />
				</Teleport>
			</ClientOnly>
		</div>
	</NuxtLayout>
</template>
<script setup lang="ts">
import type DataTableI from '~/interfaces/datatable';
import type NavItemI from '~/interfaces/navItem';
import type sliderItemI from '~/interfaces/sliderItem';
import slide1 from '@/assets/images/slide1.png';
import { useRoute } from 'vue-router';
import { formatDate, removeHtmlTags } from '~/utils/utils';
import { storeToRefs } from 'pinia';
import { useDataStore } from '@/stores/dataStore';

const route = useRoute();
const dataStore = useDataStore();
const { refreshKey } = storeToRefs(dataStore); 

const showCreateTermModal = ref(false);
const showCreateGlossaryModal = ref(false);
const showDeleteModal = ref(false);
const selectedRow = ref(null);
const isEditMode = ref(false);

const close = () => {
	if(showCreateTermModal){
		showCreateTermModal.value = false;
	}
	if(showCreateGlossaryModal){
		showCreateGlossaryModal.value = false;
		isEditMode.value = false;
	}
};

const handleEditClick = () => {
	showCreateGlossaryModal.value = true;
	isEditMode.value = true;
};

const closeDelete = () => {
    showDeleteModal.value = false;
};

const openDeleteModal = () => {
	showDeleteModal.value = true;
};

/** GLOSSARY - get glossary details **/

const glossaryDetails = ref<any>(null);

// Define Breadcrumb interface
interface BreadcrumbI {
	title: string;
	link: string;
}
const breadcrumb = ref<any>([]);

const terms = ref<any>([]);

async function fetchTermDetails(termId: string) {
	try {
		const response = await useFetchAuth(`/terms/${termId}`);
		breadcrumb.value = [
			{
				title: 'Glossary',
				link: '/glossary'
			},
			{
				title: glossaryDetails.value?.name,
				link: '#'
			}
		]
		return response || {}; // Return the term details
	} catch (error) {
		console.error('Error fetching term details:', error);
		return {}; // Return an empty object in case of error
	}
}

const fetchData = async () => {
	console.log('fetchData', route.params.id);
	if (route.params.id) {
		try {
			const response = await useFetchAuth(`/glossaries/${route.params.id}`);
			glossaryDetails.value = response || {};
			data.value.rows = glossaryDetails.value?.terms || [];
			// Update each term with type and fetch additional details
			const termPromises = data?.value?.rows?.map(async (term: any) => {
				term.type = "Term";
				// Fetch additional details for each term
				const termDetails = await fetchTermDetails(term.id);
				return { ...term, ...termDetails };
			}) || [];

			// Wait for all term details to be fetched
			if (data.value) { // Ensure data.value is defined
				data.value.rows = await Promise.all(termPromises);
			}

		} catch (error) {
			console.error('Error fetching data:', error);
			glossaryDetails.value = {}; // Set to an empty object to prevent issues
		}
	}
}


onMounted(() => {
	fetchData(); 
});

onBeforeMount(() => {
    fetchData();
});

watch(() => route.params.id, () => {
	fetchData();
}, { immediate: true });

watch(refreshKey, async(newKey) => {
	console.log('refreshKey', newKey);
	await fetchData();
});

const viewDefault = () => {
	console.log('show default home page');
}

const dummyClick = () => {
	console.log('dummyClick');
}

const goToGlossary = () => {
	navigateTo('/glossary');
}

const goToAllTerms = () => {
	navigateTo('/glossary/terms');
}

const navItems = ref<NavItemI[]>([
	{
		label: 'Glossary',
		icon: 'vocabularies_nav',
		action: goToGlossary,
		active: true
	}, {
		label: 'All terms',
		icon: 'clock',
		action: goToAllTerms,
	},
	{
		label: 'Recently viewed',
		icon: 'clock',
		action: dummyClick
	},
	{
		label: 'Highlighted',
		icon: 'favourite',
		action: dummyClick
	},
	{
		label: 'Your Catalog',
		icon: 'loading',
		action: dummyClick
	}
]);

const BannerCta = ref<sliderItemI[]>([
	{
		id: 1,
		title: 'Have a question or doubts?',
		text: 'Check our FAQs and Documentation',
		button_text: 'Faqs and Documentation',
		button_link: 'https://www.google.com',
		media_type: 1,
		media_url: slide1,
		template: 1,
	}
])

const data = ref<DataTableI>({
	options: {
		tableName: 'glossary-table',
		detailsPath: '/glossary/terms/', 
		allowCheckRow: true,
		favourites: true,
		previewComponent: 'TermsDetails', 
	},
	columns: [
		{
			id: 1,
			name: 'name',
			label: 'Name',
			combineColumn: 'type',
			renderComponent: 'TableColumnsName',
			// width: '200px'
		},
		{
			id: 4,
			name: 'summary',
			label: 'Description',
			width: '60%'
		},
		{
            id: 6,
            name: 'actions',
            label: 'Actions',
            width: '80px',
            hideLabel: true
        }
	],
	rows: []
});

</script>