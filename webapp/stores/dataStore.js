import { defineStore } from 'pinia';

// store to refresh data after add/edit/delete
export const useDataStore = defineStore('dataStore', {
  state: () => ({
    refreshKey: 0, // Used to trigger reactivity
  }),
  actions: {
    triggerRefresh() {
      this.refreshKey += 1; // Increment refreshKey to trigger reactivity
    },
  },
});

// store type of artifact
export const useArtifactStore = defineStore('artifact', {
  state: () => ({
    artifactType: null,
  }),
  actions: {
    setArtifactType(type) {
      this.artifactType = type;
    },
    clearArtifactType() {
      this.artifactType = null;
    }
  }
});