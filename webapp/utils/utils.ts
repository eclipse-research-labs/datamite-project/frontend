/**
 * Copyright (c) 2024 LOBA
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * SPDX-License-Identifier: MIT
 * 
 * Contributors:
 *     [name] - [contribution]
 */

export function formatDate(dateString: string | undefined): string {
    if (!dateString) return '';
  
    const options: Intl.DateTimeFormatOptions = {
      year: 'numeric',
      month: 'short',
      day: '2-digit',
      hour: '2-digit',
      minute: '2-digit',
      second: '2-digit',
      hour12: false,
    };
  
    return new Date(dateString).toLocaleString('en-US', options).replace(',', '');
}

export function removeHtmlTags(description: string | undefined) {
  return description?.replace(/<\/?[^>]+(>|$)/g, "");
}

export function generateUUID() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    const r = Math.random() * 16 | 0;
    const v = c === 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}

export const highlightInvalidFields = (form: HTMLFormElement) => {
  // Get all form fields (valid and invalid)
  const allFields = form.querySelectorAll('input, textarea, select');

  allFields.forEach((field) => {
      const input = field as HTMLElement;

      // Clear any previous error messages or highlights
      clearValidationError(input);

      // Check if the field is invalid
      if (field instanceof HTMLInputElement) {
        if (!field.checkValidity()) {
            // Add a class or inline styling to highlight invalid fields
            input.classList.add('error-highlight');

            // Optionally, focus on the first invalid field
            input.scrollIntoView({ behavior: 'smooth', block: 'center' });

            // Display the validation message
            const customMessage = field.getAttribute('data-error-message') || field.validationMessage;
            displayCustomErrorMessage(input, customMessage);
        }
      }
  });
};

// Function to display custom error messages
export const displayCustomErrorMessage = (field: HTMLElement, message: string) => {
  let errorElement = field.nextElementSibling as HTMLElement;

  // Check if the next sibling is already an error message, if not, create one
  if (!errorElement || !errorElement.classList.contains('error-message')) {
      errorElement = document.createElement('div');
      errorElement.className = 'error-message';
      field.parentNode?.insertBefore(errorElement, field.nextSibling);
  }

  // Set the custom message
  errorElement.textContent = message;
};

// Function to clear validation error styles and messages from valid fields
export const clearValidationError = (field: HTMLElement) => {
  // Remove the error-highlight class if it exists
  field.classList.remove('error-highlight');

  // Remove the custom error message, if any
  const errorElement = field.nextElementSibling as HTMLElement;
  if (errorElement && errorElement.classList.contains('error-message')) {
      errorElement.remove();
  }
};

export const clearAllValidationErrors = (form: HTMLFormElement) => {
  // Find all elements with the 'error-highlight' class (or other error classes you might use)
  const invalidFields = form.querySelectorAll('.error-highlight');
  
  // Iterate over all invalid fields and clear their validation error
  invalidFields.forEach((field) => {
      clearValidationError(field as HTMLElement);
  });
};

export const updateLoadingUI = (isLoading: boolean) => {

  if (isLoading) {
      document.getElementById('loading-spinner')?.classList.remove('hidden');
  } else {
      document.getElementById('loading-spinner')?.classList.add('hidden');
  }
};